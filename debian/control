Source: python-neutron-lib
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Mickael Asseline <mickael@papamica.com>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-coverage,
 python3-fixtures,
 python3-hacking,
 python3-keystoneauth1,
 python3-mock <!nodoc>,
 python3-netaddr,
 python3-openstackdocstheme <!nodoc>,
 python3-os-api-ref <!nodoc>,
 python3-os-ken,
 python3-os-traits,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.db,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.messaging,
 python3-oslo.policy (>= 3.6.2),
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.utils,
 python3-oslo.versionedobjects,
 python3-oslotest,
 python3-osprofiler,
 python3-pecan,
 python3-setproctitle,
 python3-sqlalchemy,
 python3-stestr,
 python3-stevedore,
 python3-testresources,
 python3-testscenarios,
 python3-testtools,
 python3-webob,
 subunit,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-neutron-lib
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-neutron-lib.git
Homepage: http://www.openstack.org/

Package: python-neutron-lib-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Neutron shared routines and utilities - doc
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 Neutron-lib is an OpenStack library project used by Neutron, Advanced
 Services, and third-party projects to provide common functionality and remove
 duplication.
 .
 This package contains the documentation of neutron-lib.

Package: python3-neutron-lib
Architecture: all
Depends:
 python3-keystoneauth1,
 python3-netaddr,
 python3-os-ken,
 python3-os-traits,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.db,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.messaging,
 python3-oslo.policy (>= 3.6.2),
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.utils,
 python3-oslo.versionedobjects,
 python3-osprofiler,
 python3-pbr,
 python3-pecan,
 python3-setproctitle,
 python3-sqlalchemy,
 python3-stevedore,
 python3-webob,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-neutron-lib-doc,
Description: Neutron shared routines and utilities - Python 3.x
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 Neutron-lib is an OpenStack library project used by Neutron, Advanced
 Services, and third-party projects to provide common functionality and remove
 duplication.
 .
 This package provides shared routines and utilities for Python 3.x.
